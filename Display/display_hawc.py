import pandas as pd
import matplotlib.pyplot as plt
import math as math
import matplotlib as mpl
import numpy as np

print("Espere, puede tardar..............")
#========================
#CARGAR DATOS
dat="Sim_O16" #Nombre del archivo sin .txt
#====================================


PMT,Time,A=np.loadtxt(dat+".txt",skiprows=0,unpack=True)
datos=pd.read_csv("pmt-layout-03.txt",header=0,delim_whitespace=True)
datos.rename(columns={'o': 'pmt', '0.1': 'tanque','0.2': 'n1','0.3': 'n2',
                      '0.4': 'x','0.5': 'y','0.6': 'z'}, inplace=True)
#ENCUENTRA PMT'S
x=[]
y=[]
for i in range (0,1200):
    x.append(int(datos.loc[i,"x"]-9065.23))
    y.append(int(datos.loc[i,"y"]-25936.63))
#TANQUES
ss=[]
jj=[]
for i in range(0,300):
    ss.append(x[4*(i+1)-2])
    jj.append(y[4*(i+1)-2])

#COLORES

c=["darkblue","blue","steelblue","cyan","springgreen","yellow","orange","darkorange","red","darkred"]
Hit = len(A)
colorr=["white"]*1200
for k in range (0,Hit):
    for l in range (0,1200):
        if l == (PMT[k]-1):
            cl=Time[k]/100
            if cl<10:
                colorr[l]=c[int(math.floor(cl))]
            else:
                pass

#GRAFICA
fig, ax = plt.subplots(figsize=(15,12))
ax.set_title("HAWC DISPLAY \n "+dat,fontsize=22)
plt.xlabel("Survey x[cm]", fontsize=20)
plt.ylabel("Survey y[cm]", fontsize=20)
plt.scatter(ss,jj,s=800,color='gray')
plt.scatter(x, y, s=30,color=colorr)
#barra de colores
ax1 = fig.add_axes([0.92, 0.16, 0.02, 0.69])
cmap = mpl.cm.jet
norm = mpl.colors.Normalize(vmin=0, vmax=900)
cb1 = mpl.colorbar.ColorbarBase(ax1, cmap=cmap,norm=norm,orientation='vertical')
cb1.set_label('Time[ns]',fontsize=16)

#plt.savefig('fig2.pdf') #para pdf
plt.savefig(dat+".png") #para .png
plt.show()